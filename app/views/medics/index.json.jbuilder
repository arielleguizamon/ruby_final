json.array!(@medics) do |medic|
  json.extract! medic, :id, :name, :surname, :license_number, :specialization
  json.url medic_url(medic, format: :json)
end
