class AssociationMedicTurn < ActiveRecord::Migration
  def change
    add_reference :turns ,:medic, index: true, foreign_key: true
  end
end
